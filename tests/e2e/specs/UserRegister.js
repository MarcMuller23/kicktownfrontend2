// https://docs.cypress.io/api/introduction/api.html

describe('Login System Tests', () => {
    it('Visits the app root url', () => {
        cy.visit('http://localhost:3030/#/Register')
        cy.contains('h1', 'Register')
    });
    it('Registers the user', () => {
        cy.visit('http://localhost:3030/#/Register')
        cy.get('#username').type('hello')
        cy.get('#password').type('world')
        cy.get('#login-button').click()
        cy.contains('h1', 'Sign In');
    })
})




