// https://docs.cypress.io/api/introduction/api.html

describe('Login System Tests', () => {
    it('Visits the app root url', () => {
        cy.visit('http://localhost:3030/#/Login')
        cy.contains('h1', 'Sign In')
    });
    it('signs the user in', () => {
        cy.visit('http://localhost:3030/#/Login')
        cy.get('#username').type('hello')
        cy.get('#password').type('world')
        cy.get('#login-button').click()
        cy.contains('h1', 'Latest News');
    })
})


describe('Login Fail Test', () => {
    it('Visits the app root url', () => {
        cy.visit('http://localhost:3030/#/Login')
        cy.contains('h1', 'Sign In')
    });
    it('does not log the user in', () => {
        cy.visit('http://localhost:3030/#/Login')
        cy.get('#username').type('Fout')
        cy.get('#password').type('Wachtwoord')
        cy.get('#login-button').click()
        cy.contains('.swal2-title', 'Error');
    })
})

